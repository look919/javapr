
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Tomek
 */
public class Rand extends Component {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int numMin;
    private int numMax;
    private int result;


    public int printResult() {      
       result = ThreadLocalRandom.current().nextInt(numMin, numMax);
        
       return result;
    }   

    public void setNumMin(int numMin) {
        this.numMin = numMin;
    }

    public void setNumMax(int numMax) {
        this.numMax = numMax;
    }
}

