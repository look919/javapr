/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.*;

/**
 *
 * @author Tomasz
 */
public class BlockArrow extends Component {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrowDirection direction;
    
    public enum ArrowDirection {LEFT, RIGHT, UP, DOWN;}
    public BlockArrow(){
        setPreferredSize(new Dimension(60,60));
        setForeground(Color.CYAN);
        direction = ArrowDirection.LEFT;
    }
    public synchronized void paint(Graphics g){
        Dimension d = getSize();
        int w =d.width-1;
        int h = d.height-1;
        int[] xPoints = {0,0,w};
        int[] yPoints = {h,0,h/2};
        switch (direction) {
            case LEFT: 
                xPoints = new int[]{w,w,0};
                break;
            case UP:   
                xPoints = new int[]{w,0,w/2};
                yPoints = new int[]{h,h,0};
                break;
            case DOWN: 
                xPoints = new int[]{w,0,w/2};
                break;
            default:
                break;
        }  
        
        g.fillPolygon(xPoints, yPoints,3);     
    }
    public ArrowDirection getDirection()   
    { return direction; }
    public void setDirection(ArrowDirection direction){  
        ArrowDirection oldDirection = this.direction;
        this.direction = direction;   
        firePropertyChange("direction", oldDirection, direction);
    }
    
}
